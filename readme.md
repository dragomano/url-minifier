# URL Minifier (Сервис коротких ссылок со статистикой переходов)
## Демонстрационный проект на Laravel 5

### Полное описание задания
* Пользователю предоcтавляется поле для ввода URL, по нажатию кнопки «Уменьшить» пользователю предоставляется **короткая ссылка** с текущим доменом сайта (вида http:///aBcD).
* При переходе по уменьшенной ссылке пользователь будет перенаправлен на исходную страницу.
* Пользователь должен иметь возможность создать свою короткую ссылку.
* Пользователь должен иметь возможность создавать ссылки с ограниченным сроком жизни.

Как бонус​: Пользователь, создающий ссылку также получает ссылку на _статистику переходов_. В статистике должна отображаться география переходов, данные из юзер агентов переходящих (можно использовать google charts).

Решить поставленную задачу используя:
* Laravel
* PSR2 (http://www.phpfig.org/psr/psr2/) и PSR4 (http://www.phpfig.org/psr/psr4/)
* https://getcomposer.org/ для автолоада классов и подключения сторонних библиотек, используемых для решения задачи (написанных вами в том числе)
* http://getbootstrap.com/ для стилизации HTML страниц

===============================

[Демонстрация проекта](https://i.ibb.co/SNWsTRr/minifier-url-example-1.png)

### Публикация проекта
Для развертывания проекта на вашем домене переименуйте файл **.env.example** в **.env**, выполните команду `php artisan key:generate`, укажите в _.env_ настройки подключения к базе данных, создайте таблицу в базе данных (`php artisan migrate`). После этого проверьте работу минификатора.

#### Дамп таблиц:
```sql
CREATE TABLE IF NOT EXISTS `short_urls` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `long_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `lifetime_until` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `short_urls_short_code_unique` (`short_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `visitors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url_id` bigint(20) unsigned NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `visitors_url_id_foreign` (`url_id`),
  CONSTRAINT `visitors_url_id_foreign` FOREIGN KEY (`url_id`) REFERENCES `short_urls` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
```

### Срок действия ссылок
Для удаления ссылок с истекшим сроком действия непосредственно через Laravel наберите команду `php artisan remove_old_urls`
Либо добавьте в cron запись вида `/usr/local/bin/php /path/to/artisan schedule:run >> /dev/null 2>&1`
Тогда эта задача будет запускаться ежедневно в полночь.

### Логика минификатора
```
	пользователь добавляет ссылку
		ссылка не существует в базе
			создать новую запись в таблице
		ссылка уже существует в базе
			существующий срок хранения совпадает с добавляемым (NULL или конкретная дата)
				показать готовую ссылку
			существующий срок хранения не совпадает с добавляемым
				создать новую запись в таблице
```

### Статистика
Для просмотра статистики переходов достаточно добавить /stat/ перед коротким кодом ссылки: `http://домен/stat/код`.
Для отображения стран и городов используется компонент [GeoIP for Laravel 5](https://github.com/Torann/laravel-geoip), установлен город по умолчанию — Караганда (для локальных и неопределенных IP).
