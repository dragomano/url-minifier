    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            @if (Request::path() != '/')
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ __('messages.project_title') }}
            </a>
            @else
                <h1 class="h4"><span class="navbar-brand" style="padding-bottom: 0">{{ __('messages.project_title') }}</span></h1>
            @endif
            <div class="collapse navbar-collapse">
                <span class="navbar-text">{{ __('messages.project_slogan') }}</span>
            </div>
        </div>
    </nav>