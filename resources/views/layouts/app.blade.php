<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('css/app.css', 'assets') }}">
    <title>{{ __('messages.project_title') }}</title>
</head>
<body>
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-md-12">
                @include('layouts.nav')
                @include('layouts.messages')
                @yield('content')
            </div>
        </div>
    </div>

    <script src="{{ mix('js/app.js', 'assets') }}"></script>
    @yield('scripts')

</body>
</html>