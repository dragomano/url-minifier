@if (session('success'))
    <div class="alert alert-success" role="alert">
        {{ session('success') }}
    </div>
@endif
@if (session('error'))
    <div class="alert alert-danger" role="alert">
        {{ session('error') }}
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (session('result'))
    <div class="alert alert-info" role="alert">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="short_link">{{ __('messages.short_link') }}</label>
                <input id="short_link" class="form-control" type="text" value="{{ session('result') }}" readonly>
            </div>
            <div class="form-group col-md-6">
                <label for="stat_link">{{ __('messages.click_stat') }}</label>
                <input id="stat_link" class="form-control" type="text" value="{{ session('stat') }}" readonly>
            </div>
        </div>
    </div>
@endif