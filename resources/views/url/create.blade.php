@extends('layouts.app')

@section('content')
    <div class="card bg-light">
        <div class="card-body">
            <form action="{{ route('create_short_url') }}" method="post">
                @csrf

                <div class="form-group">
                    <label for="long_url">{{ __('messages.enter_url') }}</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-link"></i>
                            </div>
                        </div>
                        <input id="long_url" name="long_url" type="text" value="{{ old('long_url') }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="lifetime">{{ __('messages.lifetime') }}</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-times"></i>
                            </div>
                        </div>
                        <input id="lifetime" name="lifetime" type="date" class="form-control" value="{{ old('lifetime') }}" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
                    </div>
                </div>
                <div class="form-group text-center">
                    <button name="submit" type="submit" class="btn btn-primary shadow-lg">{{ __('messages.minify') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection