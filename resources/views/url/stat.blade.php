@extends('layouts.app')

@section('content')
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card bg-light">
                    <div class="card-header">
                        <h1 class="h4">{{ __('messages.click_stat') }}</h1>
                    </div>
                    <div class="card-body">
                        {!! __('messages.stat_message', ['link' => str_replace('/stat', '', request()->url()), 'hits' => $url->hits]) !!}
                    </div>
                </div>
                <table class="table table-bordered table-hover text-center mt-4">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">IP</th>
                            <th scope="col">User agent</th>
                            <th scope="col">{{ __('messages.country') }}</th>
                            <th scope="col">{{ __('messages.city') }}</th>
                            <th scope="col">{{ __('messages.date') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php
                        $i = 0
                    @endphp
                    @foreach ($url->visitors as $visitor)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $visitor->ip }}</td>
                            <td>{{ $visitor->user_agent }}</td>
                            <td>{{ geoip($visitor->ip)->country }}</td>
                            <td>{{ geoip($visitor->ip)->city }}</td>
                            <td>{{ $visitor->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@stop