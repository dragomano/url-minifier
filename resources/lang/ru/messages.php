<?php

return [
    'project_title' => 'Минификатор URL',
    'project_slogan' => 'Уменьшайте любые длинные ссылки с помощью этого простого сервиса!',
    'enter_url' => 'Введите свой URL',
    'lifetime' => 'Срок хранения ссылки',
    'minify' => 'Уменьшить',
    'site_does_not_exist' => 'Указанный сайт не существует!',
    'success_message' => 'URL успешно уменьшен!',
    'short_link_warning' => ' короткая ссылка перестанет действовать.',
    'short_link' => 'Ваша короткая ссылка',
    'click_stat' => 'Статистика переходов',
    'stat_message' => 'Всего переходов по ссылке <a href=":link">:link</a>: :hits',
    'country' => 'Страна',
    'city' => 'Город',
    'date' => 'Дата',
    'page' => 'Страница',
    'page_not_found' => 'Страница не найдена',
    'back_to_main' => 'Вернуться <a href="/">на главную</a>.',
    'required_url' => 'Укажите URL для обработки'
];
