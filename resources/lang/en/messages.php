<?php

return [
    'project_title' => 'URL Minifier',
    'project_slogan' => 'Minify any long links with our simple service!',
    'enter_url' => 'Enter your URL',
    'lifetime' => 'Lifetime of link',
    'minify' => 'Minify',
    'site_does_not_exist' => 'The specified site does not exist!',
    'success_message' => 'URL successfully minified!',
    'short_link_warning' => ' the short link will stop working.',
    'short_link' => 'Your short link',
    'click_stat' => 'Statistics of clicks',
    'stat_message' => 'Total amount of click on the link <a href=":link">:link</a>: :hits',
    'country' => 'Country',
    'city' => 'City',
    'date' => 'Date',
    'page' => 'Page',
    'page_not_found' => 'Page not found',
    'back_to_main' => 'Return <a href="/">to the main</a>.',
    'required_url' => 'You must fill URL field'
];
