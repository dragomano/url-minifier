<?php

namespace App\Console\Commands;

use App\Url;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RemoveOldUrls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove_old_urls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ежедневное удаление устаревших ссылок';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Url::whereDate('lifetime_until', '<', Carbon::today())
            ->delete();
    }
}
