<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Url extends Model
{
    public $timestamps = false;
    protected $table = 'short_urls';
    protected $fillable = ['long_url', 'short_code', 'created_at', 'lifetime_until'];
    protected $dates = ['created_at', 'lifetime_until'];

    public function visitors()
    {
        return $this->hasMany(Visitor::class, 'url_id');
    }

    /**
     * При получении значения столбца lifetime_until преобразуем дату из формата Carbon в формат 'Y-m-d'
     *
     * @param $value
     *
     * @return string|null
     */
    public function getLifetimeUntilAttribute($value)
    {
        if ($value != null)
            return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('Y-m-d');

        return null;
    }

    /**
     * Проверка существования короткой ссылки для заданного сайта
     *
     * @param $url
     *
     * @return bool|mixed
     */
    public function verifyUrlExistInDb($url)
    {
        $result = $this->where('long_url', $url)->get();

        return (empty($result)) ? false : $result;
    }

    /**
     * Генерация уникального короткого кода в виде отдельной функции (для удобного изменения в будущем)
     *
     * @return string
     */
    public function createShortCode()
    {
        $short_code = Str::random(6);

        if ($this->where('short_code', $short_code)->exists())
            $short_code = $this->createShortCode();

        return $short_code;
    }

    /**
     * При запросе даты окончания хранения ссылки преобразуем её в удобный нам вид
     *
     * @param $date
     *
     * @return string
     */
    public function getNormalDate($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d.m.Y');
    }

    /**
     * Получаем короткую ссылку
     *
     * @param $code
     *
     * @return string
     */
    public function getShortLink($code)
    {
        return request()->root() . '/' . $code;
    }

    /**
     * Получаем ссылку на статистику переходов по указанному короткому коду
     *
     * @param $code
     *
     * @return string
     */
    public function getStatLink($code)
    {
        return request()->root() . '/stat/' . $code;
    }
}
