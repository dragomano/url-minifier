<?php

namespace App\Http\Requests;

use App\Url;
use Illuminate\Foundation\Http\FormRequest;

class StoreUrlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'long_url' => 'bail|required|url|max:255',
            'lifetime' => 'nullable|date'
        ];
    }

    public function messages()
    {
        return [
            'long_url.required' => __('messages.required_url')
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->long_url != null && $this->verifyUrlExist($this->long_url) === false) {
                $validator->errors()->add('long_url', __('messages.site_does_not_exist'));
            }
        });
    }

    /**
     * Проверка сайта с адресом $url на существование
     *
     * @param $url
     *
     * @return bool
     */
    public function verifyUrlExist($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch,  CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return (!empty($response) && $response != 404);
    }
}
