<?php

namespace App\Http\Controllers;

use App\Url;
use Illuminate\Http\Request;

class StatController extends Controller
{
    public function show($code)
    {
        $url = Url::whereShortCode($code)->firstOrFail();

        return view('url.stat', compact('url'));
    }
}
