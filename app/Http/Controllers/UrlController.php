<?php

namespace App\Http\Controllers;

use App\Url;
use App\Visitor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUrlRequest;
use Illuminate\Support\Facades\Validator;

class UrlController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('url.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUrlRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUrlRequest $request)
    {
        // Получим отвалидированные данные
        $data = $request->validated();

        $url = new Url;
        $url->fill($data);

        $success_message = __('messages.success_message');

        // Проверим, а вдруг для этой ссылки уже создана ранее короткая версия?
        $readyUrls = $url->verifyUrlExistInDb($data['long_url']);
        if ($readyUrls->isEmpty()) {
            $url->short_code = $url->createShortCode();
            $url->created_at = Carbon::now();

            if ($data['lifetime'] != null) {
                $url->lifetime_until = $data('lifetime');
                $success_message .= ' ' . $url->getNormalDate($data['lifetime']) . __('messages.short_link_warning');
            }

            $url->save();
        } else {
            if ($readyUrls->contains('lifetime_until', $data['lifetime'])) {
                $url->short_code = $readyUrls->where('lifetime_until', $data['lifetime'])->first()->short_code;
            } else {
                $url->short_code = $url->createShortCode();
                $url->created_at = Carbon::now();
                $url->lifetime_until = $data['lifetime'];
                $success_message .= ' ' . $url->getNormalDate($data['lifetime']) . __('messages.short_link_warning');
                $url->save();
            }
        }

        // Подготовим нашу короткую ссылку для отображения
        $short_link = $url->getShortLink($url->short_code);

        // Сформируем и сылку на статистику переходов
        $stat_link = $url->getStatLink($url->short_code);

        return redirect()
            ->back()
            ->with('success', $success_message)
            ->with('result', $short_link)
            ->with('stat', $stat_link);
    }

    /**
     * Display the specified resource.
     *
     * @param         $short_code
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function show($short_code, Request $request)
    {
        $url = Url::whereShortCode($short_code);

        // Если запрашиваемой страницы нет, показываем 404
        if ($url->first() == null) {
            abort(404);
        }

        // Увеличиваем количество переходов по ссылке на единицу
        $url->increment('hits');

        // Определяем исходный URL
        $real_url = $url->first()->long_url;

        // Вносим данные посетителя в таблицу visitors
        Visitor::create([
            'url_id' => $url->first()->id,
            'ip' => $request->getClientIp(),
            'user_agent' => $request->userAgent()
        ]);

        if ($real_url != null) {
            return redirect($real_url);
        }
    }
}
