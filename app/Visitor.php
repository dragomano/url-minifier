<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    public $timestamps = false;
    protected $fillable = ['url_id', 'ip', 'user_agent'];

    public function url()
    {
        return $this->belongsTo(Url::class);
    }
}
