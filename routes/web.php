<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UrlController@create')->name('welcome');
Route::post('/url', 'UrlController@store')->name('create_short_url');
Route::get('/{short_code}', 'UrlController@show')->name('short_code');
Route::get('stat/{code}', 'StatController@show')->name('show_url_stat');